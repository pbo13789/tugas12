import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Scanner;

public class MainFrame extends JFrame {
    final Font mainFont = new Font("Segoe print", Font.BOLD, 18);
    static JTextField FieldKodeBrg, FieldNamaBrg, FieldSatuanBrg, FieldStokBrg, FieldMinBrg;
    JLabel labelWelcome;

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/penjualan";
    static final String USER = "root";
    static final String PASS = "";

    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static Scanner inp = new Scanner(System.in);

    public void initialize() {
        JLabel labelKodeBrg = new JLabel("Kode barang");
        labelKodeBrg.setFont(mainFont);
        FieldKodeBrg = new JTextField();
        FieldKodeBrg.setFont(mainFont);

        JLabel labelNamaBrg = new JLabel("Nama Barang");
        labelNamaBrg.setFont(mainFont);
        FieldNamaBrg = new JTextField();
        FieldNamaBrg.setFont(mainFont);

        JLabel labelSatuan = new JLabel("Satuan Barang");
        labelSatuan.setFont(mainFont);
        FieldSatuanBrg = new JTextField();
        FieldSatuanBrg.setFont(mainFont);

        JLabel labelStokBrg = new JLabel("Stok Barang");
        labelStokBrg.setFont(mainFont);
        FieldStokBrg = new JTextField();
        FieldStokBrg.setFont(mainFont);

        JLabel labelMinBrg = new JLabel("Min Barang");
        labelMinBrg.setFont(mainFont);
        FieldMinBrg = new JTextField();
        FieldMinBrg.setFont(mainFont);

        JLabel labelData = new JLabel();
        labelData.setLayout(new GridLayout(5, 1, 5, 5));
        labelData.setFont(mainFont);

        // Button
        JButton btnInsert = new JButton("Insert");
        btnInsert.setFont(mainFont);
        btnInsert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String kodeBrg = FieldKodeBrg.getText();
                String namaBrg = FieldNamaBrg.getText();
                String satuanBrg = FieldSatuanBrg.getText();
                int stok = Integer.parseInt(FieldStokBrg.getText());
                int stokMin = Integer.parseInt(FieldMinBrg.getText());
                labelData.setText(
                        kodeBrg + " " + namaBrg + " " + satuanBrg + " " + stok + " " + stokMin);

                insert(kodeBrg, namaBrg, satuanBrg, stok, stokMin);

            }
        });

        JButton btnDelete = new JButton("Delete");
        btnDelete.setFont(mainFont);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FramePageDelete pd = new FramePageDelete();
                pd.Delete();
            }
        });

        JButton btnUpdate = new JButton("Update");
        btnUpdate.setFont(mainFont);
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FramePageUpdate pu = new FramePageUpdate();
                pu.Update();

            }
        });

        JButton btnShow = new JButton("Show");
        btnShow.setFont(mainFont);
        btnShow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FramePageShow ps = new FramePageShow();
                ps.show();
            }
        });

        JButton btnclr = new JButton("Clear");
        btnclr.setFont(mainFont);
        btnclr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FieldKodeBrg.setText("");
                FieldNamaBrg.setText("");
                FieldSatuanBrg.setText("");
                FieldStokBrg.setText("");
                FieldMinBrg.setText("");
                labelData.setText("");
            }
        });

        JPanel buttJPanel = new JPanel();
        buttJPanel.setLayout(new GridLayout(1, 5, 5, 5));
        buttJPanel.add(btnInsert);
        buttJPanel.add(btnDelete);
        buttJPanel.add(btnUpdate);
        buttJPanel.add(btnShow);
        buttJPanel.add(btnclr);

        JPanel formPanel = new JPanel();
        formPanel.setSize(600, 400);
        formPanel.setLayout(new GridLayout(6, 2, 5, 5));
        formPanel.add(labelKodeBrg);
        formPanel.add(FieldKodeBrg);
        formPanel.add(labelNamaBrg);
        formPanel.add(FieldNamaBrg);
        formPanel.add(labelSatuan);
        formPanel.add(FieldSatuanBrg);
        formPanel.add(labelStokBrg);
        formPanel.add(FieldStokBrg);
        formPanel.add(labelMinBrg);
        formPanel.add(FieldMinBrg);
        formPanel.add(labelData);

        JPanel mainpanel = new JPanel();
        mainpanel.setLayout(new BorderLayout());
        mainpanel.setBackground(new Color(128, 128, 255));
        mainpanel.add(formPanel, BorderLayout.NORTH);
        mainpanel.add(buttJPanel, BorderLayout.SOUTH);
        add(mainpanel);

        setTitle("CRUD");
        setSize(600, 700);
        setMinimumSize(new Dimension(300, 400));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void insert(String kodeBrg, String namaBrg, String satuanBrg, int stok, int stokMin) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Insert Beres");

            String checkQuery = "SELECT COUNT(*) AS count FROM barang WHERE kode_brg = ?";
            PreparedStatement checkPs = conn.prepareStatement(checkQuery);
            checkPs.setString(1, kodeBrg);
            ResultSet checkResult = checkPs.executeQuery();
            if (checkResult.next()) {
                int count = checkResult.getInt("count");
                if (count > 0) {
                    JOptionPane.showMessageDialog(null, "Database sudah ada");
                    System.out.println("Kode barang sudah ada dalam database");
                    checkResult.close();
                    checkPs.close();
                    conn.close();
                    return;
                }
            }
            checkResult.close();
            checkPs.close();

            stmt = conn.createStatement();

            String getMaxIdQuery = "SELECT MAX(id) AS max_id FROM barang";
            ResultSet resultSet = stmt.executeQuery(getMaxIdQuery);
            int maxId = 0;
            if (resultSet.next()) {
                maxId = resultSet.getInt("max_id");
            }
            resultSet.close();

            String insertQuery = "INSERT INTO barang (id, kode_brg, nm_brg, satuan, stok_brg, stok_min) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(insertQuery);
            ps.setInt(1, maxId + 1);
            ps.setString(2, kodeBrg);
            ps.setString(3, namaBrg);
            ps.setString(4, satuanBrg);
            ps.setInt(5, stok);
            ps.setInt(6, stokMin);

            ps.execute();

            ps.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}