import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.sql.DriverManager;

public class FramePageDelete extends MainFrame {
    JTextField FieldKolom, FieldKunciNama;

    public void Delete() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Delete");
                frame.setSize(600, 700);
                frame.setMinimumSize(new Dimension(600, 500));
                frame.setVisible(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                JPanel panel = new JPanel(new GridLayout(4, 2, 5, 5));
                panel.setOpaque(true);

                JLabel labelkolomBrg = new JLabel("Kolom Database");
                labelkolomBrg.setFont(mainFont);
                FieldKolom = new JTextField();
                FieldKolom.setFont(mainFont);

                JLabel labelNamaBrg = new JLabel("Kunci Nama Barang");
                labelNamaBrg.setFont(mainFont);
                FieldKunciNama = new JTextField();
                FieldKunciNama.setFont(mainFont);

                JLabel labelData = new JLabel();
                labelData.setLayout(new GridLayout(1, 2, 5, 5));
                labelData.setFont(mainFont);

                JButton btnDelate = new JButton("Delate");
                btnDelate.setLayout(new GridLayout(1, 2, 5, 5));
                btnDelate.setFont(mainFont);
                btnDelate.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String kolomBrg = FieldKolom.getText();
                        String namaBrg = FieldKunciNama.getText();
                        labelData.setText("Nama Kolom : " + kolomBrg + ", Key : " + namaBrg);

                        delete(kolomBrg, namaBrg);
                    }
                });

                panel.add(labelkolomBrg);
                panel.add(FieldKolom);
                panel.add(labelNamaBrg);
                panel.add(FieldKunciNama);
                panel.add(labelData);
                panel.add(btnDelate);

                frame.getContentPane().add(panel);
                frame.pack();
                frame.setLocationByPlatform(true);
                frame.setVisible(true);
                frame.setResizable(false);
            }
        });
    }

    public void delete(String kolom, String keyString) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            stmt.executeUpdate("DELETE FROM barang WHERE " + kolom + "=" + "'" + keyString + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
